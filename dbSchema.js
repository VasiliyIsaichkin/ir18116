module.exports = async (db) => {
	db.defineSchema({
		report: {
			username: 'string',
			userid  : 'integer',
			lang    : 'string',
			success : 'boolean',
			txInfo  : 'jsonb'
		}
	});

	await db.sync({force: false});
};
