const Telegraf = require('telegraf');
const config = require('nconf');
config.argv().env().file({file: 'config/config.json'});

const DB = require('osmium-db');
const OsmiumDBSession = require('./telegraf-session-osmium-db');
const BlockChain = require('./blockchain');

const delay = (ms = 750) => new Promise((resolve) => setTimeout(resolve, ms));

process.on('unhandledRejection', (reason) => {
	console.log('Error:\n', reason);
});

config.defaults({
	blockchain: {
		assetId   : '',
		useMainNet: false,
		timeout   : 3600
	},
	limit     : {
		amount: {
			min: 100000,
			max: 10000000
		}
	}
});

const amountLimits = {
	min: config.get('limit:amount:min'),
	max: config.get('limit:amount:max')
};
const cooldown = config.get('limit:cooldown');

(async () => {
	function initSession(ctx) {
		ctx.session.addr = false;
		ctx.session.amount = false;
		ctx.session.cooldown = ctx.session.cooldown || 0;
	}

	async function stageCheckCooldown(ctx) {
		if (ctx.session.cooldown > Date.now()) {
			await ctx.reply(`Нужно немного (${parseInt((ctx.session.cooldown - Date.now()) / 1000)} сек) подождать, мы не даем коины так часто!`);
			return true;
		}
	}

	async function stageAddrRequest(ctx) {
		await ctx.reply('Проверяю существование адреса...');
		try {
			await blockchain.getBalance(ctx.message.text);
		} catch (e) {
			await ctx.reply(e.data && e.data.error === 102 ? 'Введен неправильный адрес кошелка!' : `Произошла неизвестная ошибка`);
			await replyRequestAddr(ctx);
			return;
		}
		ctx.session.addr = ctx.message.text;
		await replyRequestAmount(ctx);
	}

	async function stageAmountRequest(ctx) {
		let amount = parseInt(ctx.message.text);

		if (isNaN(amount)) return await replyRequestAmount(ctx, 'Как-то введеное не очень похоже на число!');
		if (amount > amountLimits.max) return await replyRequestAmount(ctx, `Не, столько токенов не даем! Максимум - ${amountLimits.max}`);
		if (amount < amountLimits.min) return await replyRequestAmount(ctx, `Проси больше (а то комиссия больше съест)! Минимум - ${amountLimits.min}`);
		ctx.session.amount = ctx.message.text;
		await stageTryToSend(ctx);
	}

	async function stageTryToSend(ctx) {
		await delay();
		await ctx.reply(`Начинаем процесс отправки токенов (${ctx.session.amount} на адрес '${ctx.session.addr}'), ждем...`);
		let txRes = await blockchain.sendFunds(ctx.session.addr, ctx.session.amount); //test - 3MzWhv5QoWCpb3gsMicY2oCvqqtCRGCDXAE

		await db.models.report.create({
			username: ctx.message.from.username,
			userid  : ctx.message.from.id,
			lang    : ctx.message.from.language_code || 'unknown',
			success : txRes.success,
			txInfo  : txRes
		});

		if (txRes.success) {
			await ctx.replyWithPhoto('https://i.imgur.com/wqKlmxd.jpg');
			await ctx.reply(`Токены пришли! Спасибо за использование! Вот ссылка на транзакцию:`);
			await ctx.reply(txRes.link);
			ctx.session.cooldown = Date.now() + cooldown * 1000;
		} else {
			await ctx.reply(`Опаньки! У нас проблема - ${txRes.data} :(`);
		}

		initSession(ctx);
	}

	async function replyRequestAddr(ctx) {
		await delay();
		await ctx.reply(`Что-бы получить токены введи адрес кошелька в ${blockchain.useMainNet ? 'основной' : 'тестовой'} сети wavesplatform`);
	}

	async function replyRequestAmount(ctx, msg) {
		if (msg) await ctx.reply(msg);
		await delay();
		await ctx.reply(`А теперь укажи сумму (от ${amountLimits.min} до ${amountLimits.max})`);
	}


	let blockchain = new BlockChain();
	const bot = new Telegraf(config.get('telegram:token'), {polling: true});

	let db = new DB(config.get('db:name'), config.get('db:user'), config.get('db:password'));
	db.disableVirtualRemove();
	bot.use(new OsmiumDBSession(db));

	await require('./dbSchema')(db);

	bot.start(async (ctx) => {
		initSession(ctx);

		if (await stageCheckCooldown(ctx)) return;
		await ctx.replyWithPhoto('https://i.imgur.com/StcyHEZ.png');
		await ctx.reply(`Привет, ${ctx.message.from.first_name}! Этот бот раздает токены совершенно безвозмездно, т. е. даром`);
		await replyRequestAddr(ctx);
	});

	bot.on('text', async (ctx) => {
		if (await stageCheckCooldown(ctx)) return;
		if (!ctx.session.addr) return await stageAddrRequest(ctx);
		if (!ctx.session.amount) return await stageAmountRequest(ctx);
	});

	bot.startPolling();
})();