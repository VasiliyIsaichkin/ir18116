const WavesTransaction = require('waves-transactions');
const WavesAPI = require('@waves/waves-api');
const config = require('nconf');

class BlockChain {
	constructor(options = {}) {
		this.useMainNet = options.useMainNet || config.get('blockchain:useMainNet');
		this.Waves = WavesAPI.create(this.useMainNet ? WavesAPI.MAINNET_CONFIG : WavesAPI.TESTNET_CONFIG);
		this.assetId = options.assetId !== undefined ? options.assetId : config.get('blockchain:assetId');
		this.sendTimeout = options.sendTimeout !== undefined ? options.sendTimeout : (parseInt(config.get('blockchain:timeout')) || 60) * 1000;
		this.sourceSeed = options.sourceSeed || config.get('blockchain:seedPhrase');
	}

	async sendToNetwork(recipient, amount, seed) {
		let txData = await WavesTransaction.transfer({
			amount,
			recipient,
			assetId: this.assetId
		}, seed);
		return await WavesTransaction.broadcast(txData, this.useMainNet ? WavesAPI.MAINNET_CONFIG.nodeAddress : WavesAPI.TESTNET_CONFIG.nodeAddress);
	}

	async getBalance(address) {
		return await this.Waves.API.Node.addresses.balance(address);
	}

	async getBalanceBySeed(seed) {
		return await this.getBalance(this.Waves.Seed.fromExistingPhrase(seed.address).address);
	}

	async getTransactionDetails(id) {
		return await this.Waves.API.Node.transactions.get(id);
	}

	waitTransction(txDetails) {
		const betweenReq = 2000;
		let curr = 0;
		let self = this;

		return new Promise((resolve) => {
			if (!txDetails || !txDetails.id) return resolve(false);

			function _startTimer() {
				setTimeout(async () => {
					curr += betweenReq;

					let res = false;
					try {
						res = await self.getTransactionDetails(txDetails.id);
					} catch (e) {}

					if (res) return resolve(res);
					if (curr > self.sendTimeout) return resolve(null);
					_startTimer();
				}, betweenReq);
			}

			_startTimer();
		});
	}

	async sendFunds(recipient, amount) {
		let txDetails = false;

		try {
			txDetails = await this.sendToNetwork(recipient, amount, this.sourceSeed);
		} catch (e) {
			let data = `неизвестная ошибка: ${e.message}`;
			if (e.message.toLowerCase().indexOf('attempt to transfer unavailable funds') !== -1)
				data = `в кошельке бота недостаточно токенов, осталось только ${await this.getBalanceBySeed(this.sourceSeed)}`;
			if (e.message.toLowerCase().indexOf('invalid address') !== -1)
				data = 'неверный адрес получателя';
			return {success: false, data};
		}

		let data = await this.waitTransction(txDetails);
		return {success: !!data, data, link: `https://${!this.useMainNet ? 'testnet.' : ''}wavesexplorer.com/tx/${data.id}`};
	}
}

module.exports = BlockChain;