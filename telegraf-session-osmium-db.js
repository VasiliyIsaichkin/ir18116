const isPlainObject = (obj) => {
	return Object(obj) === obj && Object.getPrototypeOf(obj) === Object.prototype;
};

const getProxy = (target, id, callback) => {
	var prop;

	for (prop in target) {
		if (isPlainObject(target[prop])) {
			callback(id);
		}
	}

	return new Proxy(target, {
		set: function (obj, prop, value) {
			if (isPlainObject(value)) {
				obj[prop] = getProxy(value, id, callback);
			} else {
				obj[prop] = value;
			}

			callback(id);

			return true;
		}
	});
};

class OsmiumDBSession {
	constructor(db) {
		this.db = db;
		db.defineSchema({
			telegraf_session: {
				session: 'text'
			}
		});

		this.connect_users = [];
		this.session_ = {};

		return async (ctx, next) => {
			const id = ctx.update.message && ctx.update.message.from ? ctx.update.message.from.id : ctx.update.callback_query ? ctx.update.callback_query.from.id : false;
			if (id === false) return next();

			if (!this.connect_users.includes(id)) {
				this.connect_users.push(id);
				this.session = getProxy(this.session_, id, async (ID) => {
					await db.models.telegraf_session.update({session: JSON.stringify(this.session[ID])}, {where: {id: ID}});
					ctx.session = this.session[id];
				});
			}

			if (!this.session[id]) {
				let res = await db.models.telegraf_session.findOne({where: {id}});
				if (res) {
					ctx.session = this.session[id] = JSON.parse(res.session);
				} else {
					await db.models.telegraf_session.create({id, session: '{}'});
					ctx.session = this.session[id] = {};
				}
				next();
				return;
			}

			ctx.session = this.session[id];
			next();
		};
	}
}

module.exports = OsmiumDBSession;
